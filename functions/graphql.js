const admin = require('firebase-admin');
const app = require('express')();

admin.initializeApp();

const { ApolloServer, gql } = require('apollo-server-express');

const typeDefs = gql`
	type Hotdog {
		isKosher: Boolean
		location: String
		name: String
		style: String
		website: String
	}
	type Query {
		hotdogs: [Hotdog]
	}
`;
const resolvers = {
	Query: {
		hotdogs: () =>
			admin
				.database()
				.ref('hotdogs')
				.once('value')
				.then(snap => snap.val())
				.then(val => Object.keys(val).map(key => val[key]))
	}
};

const apollo = new ApolloServer({ typeDefs, resolvers });
apollo.applyMiddleware({ app, path: '/', cors: true });

exports.app = app