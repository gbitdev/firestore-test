const functions = require('firebase-functions');
const ssr = require('./ssr');
const graphql = require('./graphql');

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//

exports.appEntryPoint = functions.https.onRequest(ssr.server);
exports.graphql = functions.https.onRequest(graphql.app);
// exports.graphql = functions.https.onRequest(graphql.server);
